# INSTALACIÓN

Para utilizar el servicio, debe realizar lo siguiente:

1. Copiar todo el proyecto dentro de la carpeta htdocs de XAMPP (puede utilizar cualquier otro servidor que soporte PHP).
2. Crear archivos JSON describiendo a los modelos (tablas) dentro de la carpeta **input/models**.
3. Adicionalmente puede crear el modelo de los reportes dentro de la carpeta **input/reports**.
4. Puede hacer uso de la carpeta **templates** para generar la documentación de forma manual, asi como los ENDPOINTS para los reportes.
5. Todo lo que se encuentre entro de la carpeta **input/other**, se copiara al directorio **public/app** de nuestro nuevo servicio web (es por eso que contiene el archivo index.php, que será el punto de acceso al servicio).
6. Puede adicionar controladores de forma manual sobre los archivos que se encuentran en la carpeta **input/other/app/actions** (tenga cuidado en no modificar la estructura del mismo).
7. Al ingresar a la URL http://localhost/ws-creator/create-ws.php, se generara una carpeta llamada **public**, que es donde se encuentra nuestro nuevo Servicio web.
8. Listo, ya puedes consumir los datos de tus tablas, la API DOC estará disponible en la dirección: http://localhost/ws-creator/create-ws.php/public/api/.
