<?php
defined('PROJECT_PATH') OR die('Access denied');

class APICreator {
  private $content;
  private $body;
  private $tablas;
  private $reportes;
  private $acciones;
  private $cnt;

  function __construct() {
    $content = "";
    $body = "";
    $tablas = "";
    $reportes = "";
    $acciones = "";
    $cnt = 1;
  }

  function init() {
    $txt = "<!DOCTYPE html>\n";
    $txt .= "<html>\n";
    $txt .= " <head>\n";
    $txt .= "  <meta charset=\"utf-8\">\n";
    $txt .= "  <title>CV - API DOC</title>\n";
    $txt .= "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n";
    $txt .= "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";
    $txt .= "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n";
    $txt .= " </head>\n";
    $txt .= " <body>\n";
    $txt .= "  <div class=\"container\">\n";
    $txt .= "   <div class=\"row\">\n";
    $txt .= "    <div class=\"col-md-12\">\n";
    $txt .= "     <h1>CV - API DOC</h1>\n";
    $txt .= "     <div class=\"panel-group\">\n";
    $txt .= "\n";
    $this->body = $txt;
  }

  function getAccess($method, $model) {
    $sol = "";
    switch ($method) {
      case 'GET':
        foreach (explode(",", $model['access_get']) as $x) {
          $sol .= "<span class=\"label label-default\">".substr(trim($x), 4)."</span> ";
        }
        break;
      case 'POST':
        foreach (explode(",", $model['access_post']) as $x) {
          $sol .= "<span class=\"label label-default\">".substr(trim($x), 4)."</span> ";
        }
        break;
      case 'PUT':
        foreach (explode(",", $model['access_put']) as $x) {
          $sol .= "<span class=\"label label-default\">".substr(trim($x), 4)."</span> ";
        }
        break;
      case 'DELETE':
        foreach (explode(",", $model['access_delete']) as $x) {
          $sol .= "<span class=\"label label-default\">".substr(trim($x), 4)."</span> ";
        }
        break;
    }
    return $sol;
  }

  function addModel($filename) {
    $modelJSON = file_get_contents($filename);
    $model = json_decode($modelJSON, true);
    $table_name = $model['table_name'];
    $class_name = $this->camelCase($table_name);

    $txt = "      <div class=\"panel panel-info\">\n";
    $txt .= "       <div class=\"panel-heading\" data-toggle=\"collapse\" href=\"#collapse".$this->cnt."\">\n";
    $txt .= "        <span style=\"font-family:consolas\">/".$table_name."</span>\n";
    $txt .= "       </div>\n";
    $txt .= "       <div id=\"collapse".$this->cnt."\" class=\"panel-collapse collapse\">\n";
    $txt .= "        <div class=\"panel-body\">\n";
    $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-primary\" data-toggle=\"tab\" href=\"#method_get_".$this->cnt."\">GET</div>\n";
    if ($table_name != 'rol')
      $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-success\" data-toggle=\"tab\" href=\"#method_post_".$this->cnt."\">POST</div>\n";
    $txt .= "         <div class=\"tab-content\">\n";
    $txt .= "          <div id=\"method_get_".$this->cnt."\" class=\"tab-pane fade in active\">\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>GET</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('GET', $model);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>Devuelve todos los registros de la tabla <strong>$table_name</strong>.</p>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_get_output($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_get_output_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $lista = "";
    foreach ($model['atributes'] as $attribute) {
      $attribute_name = $attribute['name'];
      if (isset($attribute['values']) && isset($attribute['note'])) {
        $values = $attribute['values'];
        $note = $attribute['note'];
        $lista .= "<li class='list-group-item'><strong>$attribute_name: </strong> $values [$note]</li>\n";
      } else if (isset($attribute['values'])) {
        $values = $attribute['values'];
        $lista .= "<li class='list-group-item'><strong>$attribute_name: </strong> $values</li>\n";
      } else if (isset($attribute['note'])) {
        $note = $attribute['note'];
        $lista .= "<li class='list-group-item'><strong>$attribute_name: </strong> [$note]</li>\n";
      }
    }

    if ($lista != "") {
      $txt .= "            <div class='col-md-6'>\n";
      $txt .= "             <h4><strong>Nota.-</strong> Únicos valores posibles:</h4>\n";
      $txt .= "             <ul class='list-group'>\n";
      $txt .= $lista;
      $txt .= "             <ul>\n";
      $txt .= "            </div>\n";
    }

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";
    $txt .= "          <div id=\"method_post_".$this->cnt."\" class=\"tab-pane fade\">\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>POST</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('POST', $model);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>Crea un nuevo registro de la tabla <strong>$table_name</strong>.</p>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Entrada:</p>\n";
    $txt .= $this->pre_model_post_input($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de entrada:</p>\n";
    $txt .= $this->pre_model_post_input_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_post_output($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_post_output_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 412 status: Precondition Failed</span></h4>\n";
    $txt .= $this->pre_error(412, "Precondition Failed", "¿Estás enviando todos los datos?.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 422 status: Unprocessable Entity</span></h4>\n";
    $txt .= $this->pre_error(422, "Unprocessable Entity", "Ya existe un registro con esos datos.");
    $txt .= "            </div>\n";

    foreach ($model['foreign_keys'] as $foreign_key) {
      $table_name_for = $foreign_key['table_name'];
      $txt .= "            <div class=\"col-md-6\">\n";
      $txt .= "             <p>Posible Error:</p>\n";
      $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
      $txt .= $this->pre_error(409, "Conflict", "No existe el registro $table_name_for.");
      $txt .= "            </div>\n";
    }

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";
    $txt .= "         </div>\n";
    $txt .= "        </div>\n";
    $txt .= "       </div>\n";
    $txt .= "      </div>\n";
    $this->tablas .= $txt;
    $this->cnt = $this->cnt + 1;


    $txt = "      <div class=\"panel panel-info\">\n";
    $txt .= "       <div class=\"panel-heading\" data-toggle=\"collapse\" href=\"#collapse".$this->cnt."\">\n";
    $txt .= "        <span style=\"font-family:consolas\">/".$table_name."/:id</span>\n";
    $txt .= "       </div>\n";
    $txt .= "       <div id=\"collapse".$this->cnt."\" class=\"panel-collapse collapse\">\n";
    $txt .= "        <div class=\"panel-body\">\n";
    $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-primary\" data-toggle=\"tab\" href=\"#method_get_".$this->cnt."\">GET</div>\n";
    if ($table_name != 'rol')
      $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-warning\" data-toggle=\"tab\" href=\"#method_put_".$this->cnt."\">PUT</div>\n";
    if ($table_name != 'rol')
      $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-danger\" data-toggle=\"tab\" href=\"#method_delete_".$this->cnt."\">DELETE</div>\n";
    $txt .= "         <div class=\"tab-content\">\n";
    $txt .= "          <div id=\"method_get_".$this->cnt."\" class=\"tab-pane fade in active\">\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>GET</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('GET', $model);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>Devuelve un registro de la tabla <strong>$table_name</strong>.</p>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_get_id_output($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_get_id_output_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
    $txt .= $this->pre_error(409, "Conflict", "No existe el registro.");
    $txt .= "            </div>\n";

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";
    $txt .= "          <div id=\"method_put_".$this->cnt."\" class=\"tab-pane fade\">\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>PUT</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('PUT', $model);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>Actualiza un registro de la tabla <strong>$table_name</strong>.</p>\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Entrada:</p>\n";
    $txt .= $this->pre_model_put_id_input($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de entrada:</p>\n";
    $txt .= $this->pre_model_put_id_input_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_put_id_output($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_put_id_output_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 412 status: Precondition Failed</span></h4>\n";
    $txt .= $this->pre_error(412, "Precondition Failed", "'No colocaste el ID'");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
    $txt .= $this->pre_error(409, "Conflict", "No existe el registro.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 412 status: Precondition Failed</span></h4>\n";
    $txt .= $this->pre_error(412, "Precondition Failed", "¿Está enviando todos los datos?.");
    $txt .= "            </div>\n";

    foreach ($model['foreign_keys'] as $foreign_key) {
      $table_name_for = $foreign_key['table_name'];
      $txt .= "            <div class=\"col-md-6\">\n";
      $txt .= "             <p>Posible Error:</p>\n";
      $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
      $txt .= $this->pre_error(409, "Conflict", "No existe el registro $table_name_for.");
      $txt .= "            </div>\n";
    }

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";
    $txt .= "          <div id=\"method_delete_".$this->cnt."\" class=\"tab-pane fade\">\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>DELETE</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('DELETE', $model);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>Elimina un registro de la tabla <strong>$table_name</strong>.</p>\n";
    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_delete_id_output($model);
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= $this->pre_model_delete_id_output_example($model);
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 412 status: Precondition Failed</span></h4>\n";
    $txt .= $this->pre_error(412, "Precondition Failed", "'No colocaste el ID'");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
    $txt .= $this->pre_error(409, "Conflict", "No existe el registro.");
    $txt .= "            </div>\n";

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";
    $txt .= "         </div>\n";
    $txt .= "        </div>\n";
    $txt .= "       </div>\n";
    $txt .= "      </div>\n";
    $this->tablas .= $txt;
    $this->cnt = $this->cnt + 1;
  }

  public function pre_model_get_output($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  status: string\n";
    $txt .= "  code: int\n";
    $txt .= "  data: [\n";
    $txt .= "    {\n";
    $txt .= "      $pk: int [PK]\n";
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $txt .= "      $name: $type\n";
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $txt .= "      $name: int [FK]\n";
    }
    $txt .= "      createdAt: string\n";
    $txt .= "      updatedAt: string\n";
    $txt .= "    }\n";
    $txt .= "  ]\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  public function pre_model_get_output_example($model) {
      $pk = $model['primary_key'];
      $txt = "<pre>\n";
      $txt .= "{\n";
      $txt .= "  \"status\": \"OK\",\n";
      $txt .= "  \"code\": 200,\n";
      $txt .= "  \"data\": [\n";
      $txt .= "    {\n";
      $txt .= "      \"$pk\": 1,\n";
      foreach ($model['atributes'] as $attribute) {
        $name = $attribute['name'];
        $type = $attribute['type'];
        $example = $attribute['example'];
        if ($type=='string')  $txt .= "      \"$name\": \"$example\",\n";
        if ($type=='int')     $txt .= "      \"$name\": $example,\n";
        if ($type=='double')  $txt .= "      \"$name\": $example,\n";
      }
      foreach ($model['foreign_keys'] as $attribute) {
        $name = $attribute['name'];
        $example = $attribute['example'];
        $txt .= "      \"$name\": $example,\n";
      }
      $txt .= "      \"createdAt\": \"2017-01-23 01:07:05\",\n";
      $txt .= "      \"updatedAt\": \"0000-00-00 00:00:00\"\n";
      $txt .= "    }\n";
      $txt .= "  ]\n";
      $txt .= "}\n";
      $txt .= "</pre>\n";
      return $txt;
  }

  function pre_model_post_input($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $txt .= "  $name: $type\n";
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $txt .= "  $name: int [FK]\n";
    }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_post_input_example($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    $sw = false;
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $example = $attribute['example'];
      if ($type=='string')  $txt .= "  \"$name\": \"$example\",\n";
      if ($type=='int')     $txt .= "  \"$name\": $example,\n";
      if ($type=='double')  $txt .= "  \"$name\": $example,\n";
      $sw = true;
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $example = $attribute['example'];
      $txt .= "  \"$name\": $example,\n";
      $sw = true;
    }
    if ($sw) { $txt = substr($txt, 0, strlen($txt) - 2)."\n"; }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_post_output($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  status: string\n";
    $txt .= "  code: int\n";
    $txt .= "  data: {\n";
    $txt .= "    $pk: int [PK]\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_post_output_example($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  \"status\": \"OK\",\n";
    $txt .= "  \"code\": 200,\n";
    $txt .= "  \"data\": {\n";
    $txt .= "    \"$pk\": 1\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_get_id_output($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  status: string\n";
    $txt .= "  code: int\n";
    $txt .= "  data: {\n";
    $pk = $model['primary_key'];
    $txt .= "    $pk: int [PK]\n";
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $txt .= "    $name: $type\n";
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $txt .= "    $name: int [FK]\n";
    }
    $txt .= "    createdAt: string\n";
    $txt .= "    updatedAt: string\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_get_id_output_example($model) {
    $pk = $model['primary_key'];
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  \"status\": \"string\",\n";
    $txt .= "  \"code\": 1,\n";
    $txt .= "  \"data\": {\n";
    $txt .= "    \"$pk\": 1,\n";
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $example = $attribute['example'];
      if ($type=='string')  $txt .= "    \"$name\": \"$example\",\n";
      if ($type=='int')     $txt .= "    \"$name\": $example,\n";
      if ($type=='double')  $txt .= "    \"$name\": $example,\n";
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $example = $attribute['example'];
      $txt .= "    \"$name\": $example,\n";
    }
    $txt .= "    \"createdAt\": \"2017-01-23 01:07:05\",\n";
    $txt .= "    \"updatedAt\": \"0000-00-00 00:00:00\"\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_put_id_input($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $txt .= "  $name: $type\n";
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $txt .= "  $name: int [FK]\n";
    }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_put_id_input_example($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $sw = false;
    foreach ($model['atributes'] as $attribute) {
      $name = $attribute['name'];
      $type = $attribute['type'];
      $example = $attribute['example'];
      if ($type=='string')  $txt .= "  \"$name\": \"$example\",\n";
      if ($type=='int')     $txt .= "  \"$name\": $example,\n";
      if ($type=='double')  $txt .= "  \"$name\": $example,\n";
      $sw = true;
    }
    foreach ($model['foreign_keys'] as $attribute) {
      $name = $attribute['name'];
      $example = $attribute['example'];
      $txt .= "  \"$name\": $example,\n";
      $sw = true;
    }
    if ($sw) { $txt = substr($txt, 0, strlen($txt) - 2)."\n"; }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_put_id_output($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  status: string\n";
    $txt .= "  code: int\n";
    $txt .= "  data: {\n";
    $txt .= "   success: string\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_put_id_output_example($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  \"status\": \"OK\",\n";
    $txt .= "  \"code\": 200,\n";
    $txt .= "  \"data\": {\n";
    $txt .= "   \"success\": \"Perfecto.\"\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_delete_id_output($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  status: string\n";
    $txt .= "  code: int\n";
    $txt .= "  data: {\n";
    $txt .= "   success: string\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_model_delete_id_output_example($model) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  \"status\": \"OK\",\n";
    $txt .= "  \"code\": 200,\n";
    $txt .= "  \"data\": {\n";
    $txt .= "   \"success\": \"Perfecto.\"\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function pre_error($code, $status, $message) {
    $txt = "<pre>\n";
    $txt .= "{\n";
    $txt .= "  \"status\": \"$status\",\n";
    $txt .= "  \"code\": $code,\n";
    $txt .= "  \"data\": {\n";
    $txt .= "    \"error\": \"$message\"\n";
    $txt .= "  }\n";
    $txt .= "}\n";
    $txt .= "</pre>\n";
    return $txt;
  }

  function addReport($filename) {
    $reportJSON = file_get_contents($filename);
    $report = json_decode($reportJSON, true);
    $report_name = $report['report_name'];
    $description = $report['description'];
    $class_name = $this->camelCase($report_name);

    $txt = "      <div class=\"panel panel-warning\">\n";
    $txt .= "       <div class=\"panel-heading\" data-toggle=\"collapse\" href=\"#collapse".$this->cnt."\">\n";
    $txt .= "        <span style=\"font-family:consolas\">/".$report_name."</span>\n";
    $txt .= "       </div>\n";
    $txt .= "       <div id=\"collapse".$this->cnt."\" class=\"panel-collapse collapse\">\n";
    $txt .= "        <div class=\"panel-body\">\n";
    $txt .= "         <div type=\"button\" class=\"btn btn-xs btn-success\" data-toggle=\"tab\" href=\"#method_post_".$this->cnt."\">POST</div>\n";
    $txt .= "         <div class=\"tab-content\">\n";

    $txt .= "          <div id=\"method_post_".$this->cnt."\" class=\"tab-pane fade in active\">\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= "            <div class=\"col-xs-3 text-left\">\n";
    $txt .= "             <h3>POST</h3>\n";
    $txt .= "            </div>\n";
    $txt .= "            <div class=\"col-xs-9 text-right\">\n";
    $access = $this->getAccess('POST', $report);
    $txt .= "             <div>$access</div>\n";
    $txt .= "            </div>\n";
    $txt .= "           </div>\n";
    $txt .= "           <p>$description</p>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= $this->pre_report_post_input($report);
    $txt .= $this->pre_report_post_input_example($report);
    $txt .= "           </div>\n";

    $txt .= "           <div class=\"row\">\n";
    $txt .= $this->pre_report_post_output($report);
    $txt .= $this->pre_report_post_output_example($report);
    $txt .= "           </div>\n";


    $txt .= "           <div class=\"row\">\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 401 status: Unauthorized</span></h4>\n";
    $txt .= $this->pre_error(401, "Unauthorized", "Acceso denegado.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 412 status: Precondition Failed</span></h4>\n";
    $txt .= $this->pre_error(412, "Precondition Failed", "¿Estás enviando todos los datos?.");
    $txt .= "            </div>\n";

    $txt .= "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Posible Error:</p>\n";
    $txt .= "             <h4><span class=\"label label-danger\">code: 422 status: Unprocessable Entity</span></h4>\n";
    $txt .= $this->pre_error(422, "Unprocessable Entity", "Ya existe un registro con esos datos.");
    $txt .= "            </div>\n";

    foreach ($report['inputs'] as $input) {
      $table_name_in = $input['table_name'];
      foreach ($input['attributes'] as $attribute) {
        if (isset($attribute['is_key']) && $attribute['is_key']) {
          $txt .= "            <div class=\"col-md-6\">\n";
          $txt .= "             <p>Posible Error:</p>\n";
          $txt .= "             <h4><span class=\"label label-danger\">code: 409 status: Conflict</span></h4>\n";
          $txt .= $this->pre_error(409, "Conflict", "No existe el registro $table_name_in.");
          $txt .= "            </div>\n";
        }
      }
    }

    $txt .= "           </div>\n";

    $txt .= "          </div>\n";

    $txt .= "         </div>\n";
    $txt .= "        </div>\n";
    $txt .= "       </div>\n";
    $txt .= "      </div>\n";
    $this->reportes .= $txt;
    $this->cnt = $this->cnt + 1;
  }

  function tieneEntradas($report) {
    foreach ($report['inputs'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        $type = $attribute['type'];
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          continue;
        }
        return true;
      }
    }
    return false;
  }

  function pre_report_post_input($report) {
    if(!$this->tieneEntradas($report)) {
      return;
    }
    $txt = "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Entrada:</p>\n";
    $txt .= "<pre>\n";
    $txt .= "{\n";
    foreach ($report['inputs'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        $type = $attribute['type'];
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          continue;
        }
        $txt .= "  $name: $type\n";
      }
    }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    $txt .= "            </div>\n";
    return $txt;
  }

  function pre_report_post_input_example($report) {
    if(!$this->tieneEntradas($report)) {
      return;
    }
    $txt = "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de entrada:</p>\n";
    $txt .= "<pre>\n";
    $txt .= "{\n";
    $sw = false;
    foreach ($report['inputs'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        $type = $attribute['type'];
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          continue;
        }
        $sw = true;
        if ($type=='string')  $txt .= "  \"$name\": \"texto\",\n";
        if ($type=='int')     $txt .= "  \"$name\": 10,\n";
        if ($type=='double')  $txt .= "  \"$name\": 9.99,\n";
      }
    }
    if ($sw) { $txt = substr($txt, 0, strlen($txt) - 2)."\n"; }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    $txt .= "            </div>\n";
    return $txt;
  }

  function pre_report_post_output($report) {
    $txt = "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= "<pre>\n";
    $txt .= "{\n";
    foreach ($report['select'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['as'];
        $type = $attribute['type'];
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          continue;
        }
        $txt .= "  $name: $type\n";
      }
    }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    $txt .= "            </div>\n";
    return $txt;
  }

  function pre_report_post_output_example($report) {
    $txt = "            <div class=\"col-md-6\">\n";
    $txt .= "             <p>Ejemplo de salida:</p>\n";
    $txt .= "             <h4><span class=\"label label-success\">code: 200 status: OK</span></h4>\n";
    $txt .= "<pre>\n";
    $txt .= "{\n";
    $sw = false;
    foreach ($report['select'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['as'];
        $type = $attribute['type'];
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          continue;
        }
        $sw = true;
        if ($type=='string')  $txt .= "  \"$name\": \"texto\",\n";
        if ($type=='int')     $txt .= "  \"$name\": 10,\n";
        if ($type=='double')  $txt .= "  \"$name\": 9.99,\n";
      }
    }
    if ($sw) { $txt = substr($txt, 0, strlen($txt) - 2)."\n"; }
    $txt .= "}\n";
    $txt .= "</pre>\n";
    $txt .= "            </div>\n";
    return $txt;
  }

  function finish() {
    $this->agregarApiDocExternas();

    $txt  = "     <h3>1. Consultas Generales.</h3>\n";
    $txt .= "     <div class=\"panel-group\">\n";
    $txt .= $this->tablas;
    $txt .= "     </div>\n";
    $txt .= "     <h3>2. Reportes.</h3>\n";
    $txt .= "     <div class=\"panel-group\">\n";
    $txt .= $this->reportes;
    $txt .= "     </div>\n";
    $txt .= "     <h3>3. Acciones.</h3>\n";
    $txt .= "     <div class=\"panel-group\">\n";
    $txt .= $this->acciones;
    $txt .= "     </div>\n";

    $txt .= "    </div>\n";
    $txt .= "   </div>\n";
    $txt .= "  </div>\n";
    $txt .= "  </div>\n";
    $txt .= "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n";
    $txt .= "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n";
    $txt .= " </body>\n";
    $txt .= "</html>\n";
    $txt .= "\n";

    $this->body .= $txt;
    $this->content = $this->body;
  }

  function agregarApiDocExternas() {
    $this->reportes .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_reporte_datosPersonales.html');
    $this->reportes .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_reporte_datosPersonalesTribunal.html');
    $this->reportes .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_reporte_tribunalConvocatoria.html');
    $this->acciones .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_accion_crearCertificado.html');
    $this->acciones .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_accion_actualizarCertificado.html');
    $this->acciones .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_accion_crearDocumento.html');
    $this->acciones .= App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'APICreator_accion_crearCargo.html');
  }

  function getContent() {
    return $this->content;
  }

  function get_className() {
    return $this->class_name;
  }

  function camelCase($str)
  {
    $str = str_replace("_", " ", $str);
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    return $str;
  }
}
