<?php
defined('PROJECT_PATH') OR die('Access denied');
include_once (PROJECT_PATH.DS.'app'.DS.'ModelCreator.php');
include_once (PROJECT_PATH.DS.'app'.DS.'ControllerCreator.php');
include_once (PROJECT_PATH.DS.'app'.DS.'AppCreator.php');
include_once (PROJECT_PATH.DS.'app'.DS.'ReportCreator.php');
include_once (PROJECT_PATH.DS.'app'.DS.'APICreator.php');

class App {
  function start() {
    $this->deleteFolderPublic();
    $this->createFolders();
    $this->copyAllFilesOther();
    $this->create_app();
    $this->create_reports();
    $this->create_apidoc();

    $files = $this->getFiles(FOLDER_INPUT_MODELS);
    foreach($files as $file) {
      $this->create_model($file);
      $this->create_controller($file);
    }
  }

  function deleteFolderPublic() {
    $this->deleteDirectory(FOLDER_PUBLIC);
    echo "<li>Directorio <strong>public</strong> eliminado</li>";
  }

  function deleteDirectory($dirPath) {
    if (is_dir($dirPath)) {
      $objects = scandir($dirPath);
      foreach ($objects as $object) {
        if ($object != "." && $object !="..") {
          if (filetype($dirPath . DS . $object) == "dir") {
            $this->deleteDirectory($dirPath . DS . $object);
          } else {
            unlink($dirPath . DS . $object);
          }
        }
      }
      reset($objects);
      rmdir($dirPath);
    }
  }

  function createFolders() {
    if (!is_dir(FOLDER_PUBLIC)) { mkdir(FOLDER_PUBLIC); }
    if (!is_dir(FOLDER_PUBLIC_APP)) { mkdir(FOLDER_PUBLIC_APP); }
    if (!is_dir(FOLDER_PUBLIC_API)) { mkdir(FOLDER_PUBLIC_API); }
    if (!is_dir(FOLDER_PUBLIC_APP_CONTROLLERS)) { mkdir(FOLDER_PUBLIC_APP_CONTROLLERS); }
    if (!is_dir(FOLDER_PUBLIC_APP_MODELS)) { mkdir(FOLDER_PUBLIC_APP_MODELS); }
    if (!is_dir(FOLDER_PUBLIC_APP_REPORTS)) {   mkdir(FOLDER_PUBLIC_APP_REPORTS); }
    echo "<li>Directorio <strong>public</strong> creado</li>";
  }

  function copyAllFilesOther() {
    $this->recurse_copy(FOLDER_INPUT_OTHER, FOLDER_PUBLIC);
    echo "<li>Archivos copiados del directorio <strong>other</strong> al directorio <strong>public</strong></li>";
  }

  function recurse_copy($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
      if (($file != '.') && ($file != '..')) {
        if (is_dir($src.DS.$file)) {
          $this->recurse_copy($src.DS.$file,$dst.DS.$file);
        }
        else {
          copy($src.DS.$file,$dst.DS.$file);
        }
      }
    }
    closedir($dir);
  }

  function getFiles($folder) {
    $data = array();
    $files  = scandir($folder);
    foreach ($files as $file) {
      if (!in_array($file, array(".",".."))) {
        if (!is_dir($folder.DS.$file) ) {
          if($this->EndsWith($file, ".json")) {
            $data[] = $folder.DS.$file;
          }
        }
      }
    }
    return $data;
  }

  function create_app() {
    $app = new AppCreator();
    $app->init();
    $files = $this->getFiles(FOLDER_INPUT_MODELS);
    foreach($files as $file) {
      $app->addModel($file);
    }
    $files = $this->getFiles(FOLDER_INPUT_REPORTS);
    foreach($files as $file) {
      $app->addReport($file);
    }
    $app->finish();
    $output = fopen(FOLDER_PUBLIC_APP.DS."App.php", "w") or die("No es posible crear el archivo");
    fwrite($output, $app->getContent());
    fclose($output);
    echo "<li>Archivo generado exitosamente: <strong>".FOLDER_PUBLIC.DS."App.php</strong></li>";
  }

  function create_model($filename) {
    $app = new ModelCreator();
    $app->init();
    $app->setModel($filename);
    $app->finish();
    $outputFilename = $app->get_className().".php";
    $output = fopen(FOLDER_PUBLIC_APP_MODELS.DS.$outputFilename, "w") or die("No es posible crear el archivo");
    fwrite($output, $app->getContent());
    fclose($output);
    echo "<li>Archivo generado exitosamente: <strong>".$outputFilename."</strong></li>";
  }

  function create_controller($filename) {
    $app = new ControllerCreator();
    $app->init();
    $app->setModel($filename);
    $app->finish();
    $outputFilename = $app->get_className().".controller.php";
    $output = fopen(FOLDER_PUBLIC_APP_CONTROLLERS.DS.$outputFilename, "w") or die("No es posible crear el archivo");
    fwrite($output, $app->getContent());
    fclose($output);
    echo "<li>Archivo generado exitosamente: <strong>".$outputFilename."</strong></li>";
  }

  function create_reports() {
    $app = new ReportCreator;
    $app->init();

    $files = $this->getFiles(FOLDER_INPUT_REPORTS);
    foreach($files as $file) {
      $app->setReport($file);
    }
    $app->finish();
    $filenameController = FOLDER_PUBLIC_APP_REPORTS.DS.$app->get_className().".controller.php";
    $filenameReport = FOLDER_PUBLIC_APP_REPORTS.DS.$app->get_className().".php";

    $outputController = fopen($filenameController, "w") or die("No es posible crear el archivo");
    $outputReport = fopen($filenameReport, "w") or die("No es posible crear el archivo");
    fwrite($outputController, $app->getContentController());
    fwrite($outputReport, $app->getContentReport());
    fclose($outputController);
    fclose($outputReport);
    echo "<li>Archivo generado exitosamente: <strong>".$filenameController."</strong></li>";
    echo "<li>Archivo generado exitosamente: <strong>".$filenameReport."</strong></li>";
  }

  function create_apidoc() {
    $app = new APICreator();
    $app->init();
    $files = $this->getFiles(FOLDER_INPUT_MODELS);
    foreach($files as $file) {
      $app->addModel($file);
    }
    $files = $this->getFiles(FOLDER_INPUT_REPORTS);
    foreach($files as $file) {
      $app->addReport($file);
    }
    $app->finish();
    $output = fopen(FOLDER_PUBLIC_API.DS."index.html", "w") or die("No es posible crear el archivo");
    fwrite($output, $app->getContent());
    fclose($output);
    echo "<li>Archivo generado exitosamente: <strong>".FOLDER_PUBLIC_API.DS."index.html</strong></li>";
  }

  function EndsWith($Haystack, $Needle) {
    return strrpos($Haystack, $Needle) === strlen($Haystack)-strlen($Needle);
  }

  public static function getContentFile($filename) {
    $file = @fopen($filename, "r");
    $txt = "";
    if ($file) {
      fgets($file);
      while(! feof($file)) {
        $txt .= fgets($file);
      }
      fclose($file);
    }
    return $txt;
  }

}
