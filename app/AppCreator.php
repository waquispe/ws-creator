<?php
defined('PROJECT_PATH') OR die('Access denied');

class AppCreator {

  private $content;
  private $headers;
  private $body;

  function __construct() {
    $this->content = "";
    $this->body = "";
  }

  function init() {
    $txt = "<?php\n";
    $txt .= "defined('APP_PATH') OR die('Access denied');\n";
    $txt .= "include_once (APP_PATH.DS.'lib'.DS.'JWT.php');\n";
    $txt .= "include_once (APP_PATH.DS.'auth'.DS.'Auth.php');\n";
    $txt .= "include_once (APP_PATH.DS.'auth'.DS.'Login.controller.php');\n";
    $txt .= "include_once (APP_PATH.DS.'reports'.DS.'Reporte.controller.php');\n";
    $txt .= "include_once (APP_PATH.DS.'actions'.DS.'Accion.controller.php');\n";
    $txt .= "\n";
    $this->headers = $txt;

    $txt = "class App {\n";
    $txt .= " public function start() {\n";
    $txt .= "  if(!isset(\$_GET['action'])) {\n";
    $txt .= "   App::response_not_found();\n";
    $txt .= "  }\n";
    $txt .= "  \$action = \$_GET['action'];\n";
    $txt .= "  \$method = \$_SERVER['REQUEST_METHOD'];\n";
    $txt .= "\n";
    $txt .= "  switch (\$action) {\n";

    $this->body = $txt;
    $this->agregarRutasExternas();
  }

  function addModel($filename) {
    $modelJSON = file_get_contents($filename);
    $model = json_decode($modelJSON, true);
    $class_name = $this->camelCase($model['table_name']);
    $this->headers .= "include_once (APP_PATH.DS.'controllers'.DS.'".$class_name.".controller.php');\n";
    $table_name = $model['table_name'];
    $txt = "   case '".$table_name."':\n";
    $txt .= "    \$controller = new ".$class_name."Ctrl;\n";
    $txt .= "    switch (\$method) {\n";
    $txt .= "     case 'GET':    \$controller->get(); break;\n";
    $txt .= "     case 'POST':   \$controller->post(); break;\n";
    $txt .= "     case 'PUT':    \$controller->put(); break;\n";
    $txt .= "     case 'DELETE': \$controller->delete(); break;\n";
    $txt .= "     case 'OPTIONS': break;\n";
    $txt .= "     default:        App::response_method_not_allowed(); break;\n";
    $txt .= "    }\n";
    $txt .= "    break;\n";
    $this->body .= $txt;
  }

  function addReport($filename) {
    $modelJSON = file_get_contents($filename);
    $model = json_decode($modelJSON, true);
    $report_name = $model['report_name'];
    $class_name = "Reporte";
    $table_name = $model['report_name'];
    $txt = "   case '".$report_name."':\n";
    $txt .= "    \$controller = new ".$class_name."Ctrl;\n";
    $txt .= "    switch (\$method) {\n";
    $txt .= "     case 'POST':   \$controller->".$report_name."(); break;\n";
    $txt .= "     case 'OPTIONS': break;\n";
    $txt .= "     default:        App::response_method_not_allowed(); break;\n";
    $txt .= "    }\n";
    $txt .= "    break;\n";
    $this->body .= $txt;
  }

  function finish() {
    $this->headers .= "\n";
    $txt = "   default: App::response_not_found(); break;\n";
    $txt .= "  }\n";
    $txt .= " }\n";
    $txt .= "\n";
    $this->body .= $txt;
    $this->agregarFuncionesExternas();
    $txt = "}\n";
    $txt .= "\n";
    $this->body .= $txt;
    $this->content = $this->headers;
    $this->content .= $this->body;
  }

  function getContent() {
    return $this->content;
  }

  function agregarRutasExternas() {
    $txt = App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'AppCreator_rutas.php');
    $this->body .= $txt;
  }

  function agregarFuncionesExternas() {
    $txt = App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'AppCreator_funciones.php');
    $this->body .= $txt;
  }

  function get_className() {
    return $this->class_name;
  }

  function camelCase($str)
  {
    $str = str_replace("_", " ", $str);
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    return $str;
  }
}
