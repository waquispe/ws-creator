<?php
defined('PROJECT_PATH') OR die('Access denied');

class ModelCreator {

  private $content;
  private $model;

  private $class_name;      // Table
  private $table_name;      // table
  private $id_table;        // id_table

  private $attributes;                // col1, col2, col3, key1, key2
  private $questions;                 // ?, ?, ?, ?, ?
  private $variables;                 // $col1, $col2, $col3, $key1, $key2

  private $variables_unique;          // $col1, $col2, $key1
  private $questions_attribute;       // col1 = ?, col2 = ?, col3 = ?, key1 = ?, key2 = ?
  private $questions_attribute_and;   // col1 = ? AND col2 = ? AND key1 = ?

  function __construct() {
    $content = "";
  }

  function init() {
    $this->content = "<?php\n";
  }

  function setModel($filename) {
    $modelJSON = file_get_contents($filename);
    $model = json_decode($modelJSON, true);
    $this->model = $model;

    $this->class_name = $this->camelCase($model['table_name']);
    $this->table_name = $model['table_name'];
    $this->id_table = $model['primary_key'];

    $attributes = "";
    $questions = "";
    $variables = "";
    $variables_unique = "";
    $questions_attribute = "";
    $questions_attribute_and = "";

    foreach ($model['atributes'] as $atribute) {
      $name = $atribute['name'];

      $attributes .= $name.", ";
      $questions .= "?, ";
      $variables .= "\$$name, ";
      $questions_attribute .= $name." = ?, ";

      if ($atribute['unique']) {
        $variables_unique .= "\$$name, ";
        $questions_attribute_and .= $name." = ? AND ";
      }

    }

    foreach ($model['foreign_keys'] as $atribute) {
      $name = $atribute['name'];

      $attributes .= $name.", ";
      $questions .= "?, ";
      $variables .= "\$$name, ";
      $questions_attribute .= $name." = ?, ";

      if ($atribute['unique']) {
        $variables_unique .= "\$$name, ";
        $questions_attribute_and .= $name." = ? AND ";
      }

    }

    $this->attributes = substr($attributes, 0, strlen($attributes) - 2);
    $this->questions = substr($questions, 0, strlen($questions) - 2);
    $this->variables = substr($variables, 0, strlen($variables) - 2);
    $this->questions_attribute = substr($questions_attribute, 0, strlen($questions_attribute) - 2);
    $this->variables_unique = substr($variables_unique, 0, strlen($variables_unique) - 2);
    $this->questions_attribute_and = substr($questions_attribute_and, 0, strlen($questions_attribute_and) - 5);

    $this->add_header();
    $this->add_class_start();
    $this->add_function_getById();
    $this->add_function_getAll();
    $this->add_function_insert();
    $this->add_function_update();
    $this->add_function_delete();
    $this->add_function_existByAll();
  }

  function finish() {
    $this->add_class_end();
  }

  function getContent() {
    return $this->content;
  }

  function get_className() {
    return $this->class_name;
  }

  function camelCase($str)
  {
    $str = str_replace("_", " ", $str);
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    return $str;
  }

  function add_header() {
    $txt = "defined('APP_PATH') OR die('Access denied');\n";
    $txt .= "include_once (APP_PATH.DS.'core'.DS.'Database.php');\n";

    foreach ($this->model['foreign_keys'] as $foreign_key) {
      $filename_model = $this->camelCase($foreign_key['table_name']).".php";
      $txt .= "include_once (APP_PATH.DS.'models'.DS.'$filename_model');\n";
    }

    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_class_start() {
    $txt = "class $this->class_name extends Database {\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_class_end() {
    $txt = "}\n";
    $this->content .= $txt;
  }

  function add_function_getById() {
    $txt  = "  public static function getById(\$id) {\n";
    $txt .= "    \$query = 'SELECT * FROM $this->table_name WHERE $this->id_table = ?';\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";
    $txt .= "    \$PDO_stmt->bindParam(1, \$id, PDO::PARAM_INT);\n";
    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    \$row_count = \$PDO_stmt->rowCount();\n";
    $txt .= "    if (\$row_count != 1) {\n";
    $txt .= "      unset(\$PDO_stmt);\n";
    $txt .= "      return false;\n";
    $txt .= "    }\n";
    $txt .= "    \$row = \$PDO_stmt->fetch(PDO::FETCH_ASSOC);\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "    return \$row;\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_getAll() {
    $txt  = "  public static function getAll() {\n";
    $txt .= "    \$query = \"SELECT * FROM $this->table_name\";\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";
    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    \$row = \$PDO_stmt->fetchAll(PDO::FETCH_ASSOC);\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "    return \$row;\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_insert() {
    $txt  = "  public static function insert($this->variables) {\n";
    $txt .= "    \$query = 'INSERT INTO $this->table_name ($this->attributes) VALUES ($this->questions)';\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";

    $cnt = 1;
    foreach ($this->model['atributes'] as $atribute) {
      $atribute_name = $atribute['name'];
      if ($atribute['type'] == 'string') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_STR);\n";
      }
      if ($atribute['type'] == 'int') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      }
      if ($atribute['type'] == 'double') {
        $txt .= "    \$val = strval(\$$atribute_name);\n";
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$val, PDO::PARAM_STR);\n";
      }
      $cnt = $cnt + 1;
    }

    foreach ($this->model['foreign_keys'] as $atribute) {
      $atribute_name = $atribute['name'];
      $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      $cnt = $cnt + 1;
    }

    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    \$$this->id_table = \$PDO->lastInsertId(); \n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "    return array('$this->id_table' => \$$this->id_table);\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_update() {
    $txt  = "  public static function update(\$id, $this->variables) {\n";
    $txt .= "    \$query = 'UPDATE $this->table_name SET ".$this->questions_attribute." WHERE $this->id_table = ?';\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";

    $cnt = 1;
    foreach ($this->model['atributes'] as $atribute) {
      $atribute_name = $atribute['name'];
      if ($atribute['type'] == 'string') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_STR);\n";
      }
      if ($atribute['type'] == 'int') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      }
      if ($atribute['type'] == 'double') {
        $txt .= "    \$val = strval(\$$atribute_name);\n";
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$val, PDO::PARAM_STR);\n";
      }
      $cnt = $cnt + 1;
    }

    foreach ($this->model['foreign_keys'] as $atribute) {
      $atribute_name = $atribute['name'];
      $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      $cnt = $cnt + 1;
    }

    $txt .= "    \$PDO_stmt->bindParam($cnt, \$id, PDO::PARAM_INT);\n";
    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_delete() {
    $txt  = "  public static function delete(\$id) {\n";
    $txt .= "    \$query = 'DELETE FROM $this->table_name WHERE $this->id_table = ?';\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";
    $txt .= "    \$PDO_stmt->bindParam(1, \$id, PDO::PARAM_INT);\n";
    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }

  function add_function_existByAll() {
    if ($this->variables_unique == "") {
      return;
    }
    $txt  = "  public static function existByAll(".$this->variables_unique.") {\n";
    $txt .= "    \$query = 'SELECT * FROM $this->table_name WHERE ".$this->questions_attribute_and."';\n";
    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";

    $cnt = 1;
    foreach ($this->model['atributes'] as $atribute) {
      if (!$atribute['unique']) {
        continue;
      }
      $atribute_name = $atribute['name'];
      if ($atribute['type'] == 'string') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_STR);\n";
      }
      if ($atribute['type'] == 'int') {
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      }
      if ($atribute['type'] == 'double') {
        $txt .= "    \$val = strval(\$$atribute_name);\n";
        $txt .= "    \$PDO_stmt->bindParam($cnt, \$val, PDO::PARAM_STR);\n";
      }
      $cnt = $cnt + 1;
    }

    foreach ($this->model['foreign_keys'] as $atribute) {
      if (!$atribute['unique']) {
        continue;
      }
      $atribute_name = $atribute['name'];
      $txt .= "    \$PDO_stmt->bindParam($cnt, \$$atribute_name, PDO::PARAM_INT);\n";
      $cnt = $cnt + 1;
    }

    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    \$row_count = \$PDO_stmt->rowCount();\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "    return \$row_count >= 1;\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->content .= $txt;
  }
}
