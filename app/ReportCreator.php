<?php
defined('PROJECT_PATH') OR die('Access denied');

class ReportCreator {

  private $contentController;
  private $contentReport;

  private $headersController;
  private $bodyController;
  private $headersReport;
  private $bodyReport;

  private $report;
  private $class_name;                // Report
  private $report_name;               // report
  private $access_post;               // ROL_ADMIN, ROL_KARDEX, ROL_TRIBUNAL

  private $select;                    // tab1.col1, tab2.col1, tab3.col1
  private $from;                      // tab1, tab2, tab3
  private $where;                     // tab1.id_tab1 = tab2.id_tab1 AND tab1.id_tab1 = tab3.id_tab1
  private $inputs;                    // tab1.col1 = ? AND tab3.col1 = ?

  private $inputs_variables;          // $col1, $col2
  private $inputs_obj;                // $obj->col1, $obj->col2
  private $inputs_obj_isset;          // isset($obj->col1) && isset($obj->col2)

  function __construct() {
    $this->headersController = "";
    $this->bodyController = "";
    $this->headersReport = "";
    $this->bodyReport = "";
  }

  function init() {
    $this->class_name = "Reporte";
    $this->report_name = "reporte";
    $classneme_ctrl = "ReporteCtrl";

    $txt = "<?php\n";
    $txt .= "defined('APP_PATH') OR die('Access denied');\n";
    $txt .= "include_once (APP_PATH.DS.'core'.DS.'Controller.php');\n";
    $txt .= "include_once (APP_PATH.DS.'reports'.DS.'$this->class_name.php');\n";
    $txt .= "\n";
    $this->headersController = $txt;

    $txt = "class $classneme_ctrl extends Controller {\n";
    $txt .= "\n";
    $this->bodyController = $txt;
    $this->agregarFuncionesExternasAlControlador();

    $txt = "<?php\n";
    $txt .= "defined('APP_PATH') OR die('Access denied');\n";
    $txt .= "include_once (APP_PATH.DS.'core'.DS.'Database.php');\n";
    $txt .= "\n";
    $this->headersReport = $txt;

    $txt = "class $this->class_name extends Database {\n";
    $txt .= "\n";
    $this->bodyReport = $txt;
    $this->agregarFuncionesExternasAlModelo();
  }

  function setReport($filename) {
    $reportJSON = file_get_contents($filename);
    $report = json_decode($reportJSON, true);

    $this->report = $report;
    $this->access_post = $report['access_post'];
    $this->report_name = $report['report_name'];

    $select = "";
    $from = "";
    $where = "";
    $inputs = "";
    $inputs_variables = "";       // $col1, $col2
    $inputs_obj = "";             // $obj->col1, $obj->col2
    $inputs_obj_isset = "";       // isset($obj->col1) && isset($obj->col2)

    foreach ($report['select'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        $nameAS = $attribute['as'];
        $select .= $table_name.".$name AS $nameAS, ";
      }
    }

    foreach ($report['from'] as $table) {
      $from .= $table.", ";
    }

    foreach ($report['where'] as $table) {
      $table_name_1 = $table['table_name_1'];
      $table_name_2 = $table['table_name_2'];
      $id_table = $table['id_table'];
      $where .= $table_name_1.".".$id_table." = ".$table_name_2.".".$id_table." AND ";
    }

    foreach ($report['inputs'] as $table) {
      $table_name = $table['table_name'];
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        $inputs .= $table_name.".$name = ? AND ";
        $inputs_variables .= "\$$name, ";
        if (isset($attribute['in_token']) && $attribute['in_token']) {
          $inputs_obj .= "\$auth['$name'], ";
        } else {
          $inputs_obj .= "\$obj->$name, ";
          $inputs_obj_isset .= "isset(\$obj->$name) && ";
        }
      }
    }

    $this->select = substr($select, 0, strlen($select) - 2);
    $this->from = substr($from, 0, strlen($from) - 2);
    $this->where = substr($where, 0, strlen($where) - 5);
    $this->inputs = substr($inputs, 0, strlen($inputs) - 5);
    $this->inputs_variables = substr($inputs_variables, 0, strlen($inputs_variables) - 2);
    $this->inputs_obj = substr($inputs_obj, 0, strlen($inputs_obj) - 2);
    $this->inputs_obj_isset = substr($inputs_obj_isset, 0, strlen($inputs_obj_isset) - 4);

    $this->addController();
    $this->addReport();
  }

  function addController() {
    $txt = "  function $this->report_name() {\n";
    $txt .= "    \$auth = Auth::verify_access([$this->access_post]);\n";
    $txt .= "    if (isset(\$auth['error'])) {\n";
    $txt .= "      App::response_unauthorized(\$auth);\n";
    $txt .= "    }\n";
    $txt .= "    \$obj = \$this->get_data();\n";
    $txt .= "    if($this->inputs_obj_isset) {\n";
    $txt .= "      \$data = $this->class_name::$this->report_name($this->inputs_obj);\n";
    $txt .= "      if (isset(\$data['error'])) {\n";
    $txt .= "        App::response_unprocessable_entity(\$data);\n";
    $txt .= "      }\n";
    $txt .= "      App::response_ok(\$data);\n";
    $txt .= "    }\n";
    $txt .= "    App::response_precondition_failed();\n";
    $txt .= "  }\n";
    $txt .= "\n";
    $this->bodyController .= $txt;
  }

  function addReport() {
    $txt  = "  public static function $this->report_name($this->inputs_variables) {\n";
    foreach ($this->report['inputs'] as $table) {
      $table_name = $table['table_name'];
      $class_name = $this->camelCase($table_name);
      foreach ($table['attributes'] as $attribute) {
        $name = $attribute['name'];
        if ($attribute['is_key']) {
          $this->headersReport .= "include_once (APP_PATH.DS.'models'.DS.'$class_name.php');\n";
          $txt .= "    if (!$class_name::getById(\$$name)) {\n";
          $txt .= "      return array('error'=>'No existe el registro $table_name');\n";
          $txt .= "    }\n";
        }
      }
    }

    if ($this->where == "") {
      if ($this->inputs == "") {
        $txt .= "    \$query = 'SELECT $this->select FROM $this->from';\n";
      } else {
        $txt .= "    \$query = 'SELECT $this->select FROM $this->from WHERE $this->inputs';\n";
      }
    } else {
      if ($this->inputs == "") {
        $txt .= "    \$query = 'SELECT $this->select FROM $this->from WHERE $this->where';\n";
      } else {
        $txt .= "    \$query = 'SELECT $this->select FROM $this->from WHERE $this->where AND $this->inputs';\n";
      }
    }

    $txt .= "    \$PDO = DATABASE::instance();\n";
    $txt .= "    \$PDO_stmt = \$PDO->prepare(\$query);\n";

    foreach ($this->report['inputs'] as $table) {
      $table_name = $table['table_name'];
      $class_name = $this->camelCase($table_name);
      $cnt = 1;
      foreach ($table['attributes'] as $attribute) {
        $attribute_name = $attribute['name'];
        if ($attribute['type'] == 'string') {
          $txt .= "    \$PDO_stmt->bindParam($cnt, \$$attribute_name, PDO::PARAM_STR);\n";
        }
        if ($attribute['type'] == 'int') {
          $txt .= "    \$PDO_stmt->bindParam($cnt, \$$attribute_name, PDO::PARAM_INT);\n";
        }
        if ($attribute['type'] == 'double') {
          $txt .= "    \$val = strval(\$$attribute_name);\n";
          $txt .= "    \$PDO_stmt->bindParam($cnt, \$val, PDO::PARAM_STR);\n";
        }
        $cnt = $cnt + 1;
      }
    }

    $txt .= "    \$PDO_stmt->execute();\n";
    $txt .= "    \$row = \$PDO_stmt->fetchAll(PDO::FETCH_ASSOC);\n";
    $txt .= "    unset(\$PDO_stmt);\n";
    $txt .= "    return \$row;\n";
    $txt .= "  }\n";
    $txt .= "\n";

    $this->bodyReport .= $txt;
  }

  function agregarFuncionesExternasAlControlador() {
    $txt = App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'ReportCreator_controller_funciones.php');
    $this->bodyController .= $txt;
  }

  function agregarFuncionesExternasAlModelo() {
    $txt = App::getContentFile(FOLDER_INPUT_TEMPLATES.DS.'ReportCreator_model_funciones.php');
    $this->bodyReport .= $txt;
  }

  function finish() {
    $txt = "}\n";
    $txt .= "\n";
    $this->bodyController .= $txt;

    $this->contentController = $this->headersController;
    $this->contentController .= $this->bodyController;

    $this->headersReport .= "\n";

    $txt = "}\n";
    $txt .= "\n";
    $this->bodyReport .= $txt;

    $this->contentReport = $this->headersReport;
    $this->contentReport .= $this->bodyReport;
  }

  function getContentController() {
    return $this->contentController;
  }

  function getContentReport() {
    return $this->contentReport;
  }

  function get_className() {
    return $this->class_name;
  }

  function camelCase($str)
  {
    $str = str_replace("_", " ", $str);
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    return $str;
  }

}
