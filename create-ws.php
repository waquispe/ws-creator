<?php

define("DS", DIRECTORY_SEPARATOR);
define("PROJECT_PATH", dirname(__DIR__).DS."cv");

define("FOLDER_INPUT", PROJECT_PATH.DS.'input');

define("FOLDER_INPUT_OTHER", FOLDER_INPUT.DS.'other');
define("FOLDER_INPUT_MODELS", FOLDER_INPUT.DS.'models');
define("FOLDER_INPUT_REPORTS", FOLDER_INPUT.DS.'reports');
define("FOLDER_INPUT_TEMPLATES", FOLDER_INPUT.DS.'templates');

define("FOLDER_PUBLIC", PROJECT_PATH.DS.'public');
define("FOLDER_PUBLIC_API", FOLDER_PUBLIC.DS.'api');
define("FOLDER_PUBLIC_APP", FOLDER_PUBLIC.DS.'app');

define("FOLDER_PUBLIC_APP_MODELS", FOLDER_PUBLIC_APP.DS."models");
define("FOLDER_PUBLIC_APP_CONTROLLERS", FOLDER_PUBLIC_APP.DS."controllers");
define("FOLDER_PUBLIC_APP_REPORTS", FOLDER_PUBLIC_APP.DS."reports");

include_once (PROJECT_PATH.DS.'app'.DS.'App.php');

$app = new App;
$app->start();
