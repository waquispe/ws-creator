CREATE DATABASE cv DEFAULT CHARACTER SET UTF8 COLLATE utf8_bin;
USE cv;

CREATE TABLE rol (
	id_rol int NOT NULL AUTO_INCREMENT,
	nombre varchar (200), -- Kardex, Tribunal, Postulante
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_rol)
);

CREATE TABLE usuario (
	id_usuario  int NOT NULL AUTO_INCREMENT,
	user varchar(20) NOT NULL,
	pass varchar(50) NOT NULL,
	id_rol int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_usuario),
	FOREIGN KEY (id_rol) REFERENCES rol (id_rol)
);

CREATE TABLE convocatoria (
	id_convocatoria int NOT NULL AUTO_INCREMENT,
	titulo varchar(200),
	gestion int, -- 2010, 2012, ...
	periodo int, -- 1, 2
	url_img varchar(200),
	fecha_publicacion date,
	fecha_ex_escrito datetime,
	fecha_fin date,
	estado varchar(30), -- Borrador, Publicado
	revision_habilitada int DEFAULT 0, -- 0 = NO, 1 = SI
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_convocatoria)
);

CREATE TABLE cargo (
	id_cargo int NOT NULL AUTO_INCREMENT,
	nombre varchar(200) NOT NULL,
	materia varchar(200),
	sigla varchar(10),
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_cargo)
);

CREATE TABLE tribunal (
	id_tribunal int NOT NULL AUTO_INCREMENT,
	tipo varchar(30) NOT NULL, -- Docente, Estudiante
	id_convocatoria int NOT NULL,
	id_usuario int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_tribunal),
	FOREIGN KEY (id_convocatoria) REFERENCES convocatoria (id_convocatoria),
	FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario)
);

CREATE TABLE convocatoria_cargo (
	id_convocatoria_cargo int NOT NULL AUTO_INCREMENT,
	id_convocatoria int NOT NULL,
	id_cargo int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_convocatoria_cargo),
	FOREIGN KEY (id_convocatoria) REFERENCES convocatoria (id_convocatoria),
	FOREIGN KEY (id_cargo) REFERENCES cargo (id_cargo)
);

CREATE TABLE certificado (
	id_certificado int NOT NULL AUTO_INCREMENT,
	titulo varchar(200),
	tipo_presencia varchar(30), -- Curso Aprobado, Curso Asistido
	resolucion int, -- 0, 1
	categoria varchar(30), -- Pedagogía, Congreso, Experiencia, Participación
	estado varchar(30), -- Registrado, Enviado, Recibido, Validado, Observado, Anulado
	puntaje double,
	gestion int, -- 2009, 2012, ...
	periodo int, -- 1, 2
	id_usuario int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_certificado),
	FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario)
);

CREATE TABLE certificado_tribunal (
	id_certificado_tribunal int NOT NULL AUTO_INCREMENT,
	id_certificado int NOT NULL,
	id_tribunal int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_certificado_tribunal),
	FOREIGN KEY (id_certificado) REFERENCES certificado (id_certificado),
	FOREIGN KEY (id_tribunal) REFERENCES tribunal (id_tribunal)
);

CREATE TABLE certificado_convocatoria (
	id_certificado_convocatoria int NOT NULL AUTO_INCREMENT,
	id_certificado int NOT NULL,
	id_convocatoria int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_certificado_convocatoria),
	FOREIGN KEY (id_certificado) REFERENCES certificado (id_certificado),
	FOREIGN KEY (id_convocatoria) REFERENCES convocatoria (id_convocatoria)
);

CREATE TABLE documento (
	id_documento int NOT NULL AUTO_INCREMENT,
	nombre varchar(200) NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_documento)
);

CREATE TABLE convocatoria_documento (
	id_convocatoria_documento int NOT NULL AUTO_INCREMENT,
	id_convocatoria int NOT NULL,
	id_documento int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_convocatoria_documento),
	FOREIGN KEY (id_convocatoria) REFERENCES convocatoria (id_convocatoria),
	FOREIGN KEY (id_documento) REFERENCES documento (id_documento)
);

CREATE TABLE postulante (
	id_postulante int NOT NULL AUTO_INCREMENT,
	nota_examen_escrito double,
	nota_examen_oral double,
	fecha_reg_ex_escrito datetime,
	fecha_reg_ex_oral datetime,
	nota_final_documentacion double,
	id_usuario_postulante int NOT NULL,
	id_usuario_reg_ex_escrito int NOT NULL,
	id_usuario_reg_ex_oral int NOT NULL,
	id_convocatoria_cargo int NOT NULL,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_postulante),
	FOREIGN KEY (id_usuario_postulante) REFERENCES usuario (id_usuario),
	FOREIGN KEY (id_usuario_reg_ex_escrito) REFERENCES usuario (id_usuario),
	FOREIGN KEY (id_usuario_reg_ex_oral) REFERENCES usuario (id_usuario),
	FOREIGN KEY (id_convocatoria_cargo) REFERENCES convocatoria_cargo (id_convocatoria_cargo)
);

CREATE TABLE documento_presentado (
	id_documento_presentado int NOT NULL AUTO_INCREMENT,
	id_postulante int,
	id_convocatoria_documento int,
	createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_documento_presentado),
	FOREIGN KEY (id_postulante) REFERENCES postulante (id_postulante),
	FOREIGN KEY (id_convocatoria_documento) REFERENCES convocatoria_documento (id_convocatoria_documento)
);

INSERT INTO rol (id_rol, nombre) VALUES (1, 'Kardex');
INSERT INTO rol (id_rol, nombre) VALUES (2, 'Tribunal');
INSERT INTO rol (id_rol, nombre) VALUES (3, 'Postulante');

INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (1, 'kardex', '202cb962ac59075b964b07152d234b70', 1);	-- pass = 123
INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (2, 'tribunal1', '202cb962ac59075b964b07152d234b70', 2); -- pass = 123
INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (3, 'tribunal2', '202cb962ac59075b964b07152d234b70', 2); -- pass = 123
INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (4, 'tribunal3', '202cb962ac59075b964b07152d234b70', 2); -- pass = 123
INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (5, 'postulante2', '202cb962ac59075b964b07152d234b70', 3); -- pass = 123
INSERT INTO usuario (id_usuario, user, pass, id_rol) VALUES (6, 'postulante1', '202cb962ac59075b964b07152d234b70', 3); -- pass = 123

INSERT INTO convocatoria (id_convocatoria, titulo, gestion, periodo, url_img, fecha_publicacion, fecha_fin, estado) VALUES (1, '1ra Convocatoria de Auxiliares de Docencia', 2016, 1, 'assets/img/conv_001.jpg', '2015-04-03', '2016-05-03', 'Borrador');

INSERT INTO cargo (id_cargo, nombre, materia, sigla) VALUES (1, 'Auxiliar de Docencia', 'Introduccion a la programacion', 'INF111');
INSERT INTO cargo (id_cargo, nombre, materia, sigla) VALUES (2, 'Auxiliar de Docencia', 'Laboratorio de INF111', 'LAB111');
INSERT INTO cargo (id_cargo, nombre, materia, sigla) VALUES (3, 'Auxiliar de Docencia', 'Organización de computadoras', 'INF112');
INSERT INTO cargo (id_cargo, nombre, materia, sigla) VALUES (4, 'Auxiliar de Docencia', 'Laboratorio de Computación', 'INF113');

INSERT INTO tribunal (id_tribunal, tipo, id_Convocatoria, id_usuario) VALUES (1, 'Estudiante', 1, 2);
INSERT INTO tribunal (id_tribunal, tipo, id_Convocatoria, id_usuario) VALUES (2, 'Estudiante', 1, 3);
INSERT INTO tribunal (id_tribunal, tipo, id_Convocatoria, id_usuario) VALUES (3, 'Estudiante', 1, 4);
