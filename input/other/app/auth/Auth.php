<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'lib'.DS.'JWT.php');
include_once (APP_PATH.DS.'config'.DS.'config.php');
include_once (APP_PATH.DS.'core'.DS.'Database.php');
include_once (APP_PATH.DS.'models'.DS.'Usuario.php');

class Auth {
  public static function verify_access($array_id_roles) {
    $headers = apache_request_headers();
    if (!isset($headers['Authorization'])) {
      return array('error'=>'No se encuentra la cabecera Authorization.');
    }
    $token_encrypted = $headers['Authorization'];
    try {
      $token = JWT::decode($token_encrypted, KEY_SECRET, array('HS512'));
    } catch (Exception $e) {
      return array('error'=>'Token inválido');
    }
    if ($token->exp < time()) {
      return array('error'=>'Token expirado');
    }
    foreach ($array_id_roles as $id_rol) {
      if ($token->data->id_rol == $id_rol) {
        return get_object_vars($token->data);
      }
    }
    return array('error'=>'Acceso no autorizado');
  }

  public static function login($user, $pass) {
    $ip = $_SERVER['REMOTE_ADDR'];
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $passMD5 = md5($pass);
    if (!Usuario::existByAll($user)) {
      Log::insert($user, $pass, $ip, $browser, 0);
      return array('error'=>'Usuario y/o password incorrecto.');
    }
    $query = "SELECT * FROM usuario WHERE user = ? AND pass = ?";
		$PDO = DATABASE::instance();
		$PDO_stmt = $PDO->prepare($query);
		$PDO_stmt->bindParam(1, $user, PDO::PARAM_STR);
		$PDO_stmt->bindParam(2, $passMD5, PDO::PARAM_STR);
		$PDO_stmt->execute();
		$row_count = $PDO_stmt->rowCount();
    if ($row_count != 1) {
      unset($PDO_stmt);
      Log::insert($user, $pass, $ip, $browser, 0);
      return array('error'=>'Usuario y/o password incorrecto.');
    }
    $row = $PDO_stmt->fetch(PDO::FETCH_ASSOC);
    $usuario = array();
    $usuario['id_usuario'] = $row['id_usuario'];
    $usuario['user'] = $row['user'];
    $usuario['id_rol'] = $row['id_rol'];
    $data = array();
    $data['token'] = Auth::generateToken($usuario);
    if ($row['id_rol'] == 1) {
      $data['url'] = '/kardex/home';
      $data['rol'] = 'kardex';
    }
    if ($row['id_rol'] == 2) {
      $data['url'] = '/tribunal/home';
      $data['rol'] = 'tribunal';
    }
    if ($row['id_rol'] == 3) {
      $data['url'] = '/user/home';
      $data['rol'] = 'postulante';
    }
    unset($PDO_stmt);
    Log::insert($user, $passMD5, $ip, $browser, 1);
    return $data;
  }

  public static function generateToken($data) {
    $tokenId    = base64_encode(mcrypt_create_iv(32));
    $issuedAt   = time();                   // tiempo actual
    $expire     = $issuedAt + 86400;        // 1 dia (segundos)
    $token = [
      'iat'  => $issuedAt,         // Issued at: Tiempo en que se generó el token
      'jti'  => $tokenId,          // Json Token Id: Identificador único para el token
      'exp'  => $expire,           // Tiempo en que el token expirará
      'data' => $data
    ];
    $token_encrypted = JWT::encode($token, KEY_SECRET,'HS512');
    return $token_encrypted;
  }
}

include_once (APP_PATH.DS.'core'.DS.'Database.php');
class Log extends Database {
 public static function insert($user, $pass, $ip, $browser, $success) {
  $query = 'INSERT INTO log (user, pass, ip, browser, success) VALUES (?, ?, ?, ?, ?)';
  $PDO = DATABASE::instance();
  $PDO_stmt = $PDO->prepare($query);
  $PDO_stmt->bindParam(1, $user, PDO::PARAM_STR);
  $PDO_stmt->bindParam(2, $pass, PDO::PARAM_STR);
  $PDO_stmt->bindParam(3, $ip, PDO::PARAM_STR);
  $PDO_stmt->bindParam(4, $browser, PDO::PARAM_STR);
  $PDO_stmt->bindParam(5, $success, PDO::PARAM_INT);
  $PDO_stmt->execute();
  $id_log = $PDO->lastInsertId();
  unset($PDO_stmt);
  return array('id_log' => $id_log);
 }
}
