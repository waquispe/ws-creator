<?php
defined('APP_PATH') OR die('Access denied');
include_once (APP_PATH.DS.'lib'.DS.'JWT.php');

class Controller {
  // Devuelve el JSON del cuerpo de una petición, como un objeto (array).
  function get_data() {
    $jwt = "";
    try {
      $jwt = JWT::jsonDecode(file_get_contents('php://input'));
    } catch (Exception $e) { }
    return $jwt;
  }
}
