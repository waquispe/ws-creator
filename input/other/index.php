<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
header('Access-Control-Allow-Headers: Content-Type, Authorization');

define('ROL_KARDEX', 1);
define('ROL_TRIBUNAL', 2);
define('ROL_POSTULANTE', 3);

define("DS", DIRECTORY_SEPARATOR);
define("PROJECT_PATH", dirname(__DIR__));
define("APP_PATH", PROJECT_PATH.DS.'public'.DS."app");

require_once APP_PATH.DS.'App.php';
$app = new App();
$app->start();

?>
