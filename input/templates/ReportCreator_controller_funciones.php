<?php
function reporte_datos_personales() {
  $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
  if (isset($auth['error'])) {
    App::response_unauthorized($auth);
  }
  $datos = array();
  $datos['id_usuario'] = $auth['id_usuario'];
  $datos['nombre'] = 'Omar';
  $datos['paterno'] = 'Quispe';
  $datos['materno'] = 'Mita';
  $datos['ci'] = '12346578 L.P.';
  $datos['email'] = 'omar@gmail.com';
  $datos['fecha_nacimiento'] = '1999-02-02';
  $datos['ru'] = '123456';

  $puntajes = array();
  $puntajes['pedagogia'] = 4;
  $puntajes['congreso'] = 3;
  $puntajes['experiencia'] = 2;
  $puntajes['participacion'] = 1;
  $datos['puntajes'] = $puntajes;

  App::response_ok($datos);
}

function reporte_datos_personales_tribunal() {
  $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
  if (isset($auth['error'])) {
    App::response_unauthorized($auth['error']);
  }
  $obj = $this->get_data();
  if(!(isset($obj->ci))) {
    App::response_precondition_failed();
  }

  $datos = array();
  $datos['nombre'] = 'Omar';
  $datos['paterno'] = 'Quispe';
  $datos['materno'] = 'Mita';

  App::response_ok($datos);
}

function reporte_tribunal_convocatoria() {
  $auth = Auth::verify_access([ROL_KARDEX, ROL_TRIBUNAL, ROL_POSTULANTE]);
  if (isset($auth['error'])) {
    App::response_unauthorized($auth['error']);
  }
  $obj = $this->get_data();
  if(!(isset($obj->id_convocatoria))) {
    App::response_precondition_failed();
  }
  $data = Convocatoria::getById($obj->id_convocatoria);
  if (!$data) {
    App::response_conflict('No existe el registro convocatoria.');
  }

  $data = Reporte::reporte_tribunal_convocatoria($obj->id_convocatoria);

  $lista_tribunal = array();
  foreach ($data as $tribunal) {
    $id_tribunal = $tribunal['id_tribunal'];
    $id_convocatoria = $tribunal['id_convocatoria'];
    $id_usuario = $tribunal['id_usuario'];
    $nombre = 'Luis';
    $paterno = 'Callisaya';
    $materno = 'Mamani';

    $data_tribunal = array(
      'id_tribunal' => $id_tribunal,
      'id_convocatoria' => $id_convocatoria,
      'id_usuario' => $id_usuario,
      'nombre' => $nombre,
      'paterno' => $paterno,
      'materno' => $materno
    );

    $lista_tribunal[] = $data_tribunal;
  }

  App::response_ok($lista_tribunal);
}
