<?php
function reporte_tribunal_convocatoria($id_convocatoria) {
  $query = 'SELECT * FROM tribunal WHERE id_convocatoria = ?';
  $PDO = DATABASE::instance();
  $PDO_stmt = $PDO->prepare($query);
  $PDO_stmt->bindParam(1, $id_convocatoria, PDO::PARAM_INT);
  $PDO_stmt->execute();
  $row = $PDO_stmt->fetchAll(PDO::FETCH_ASSOC);
  unset($PDO_stmt);
  return $row;
}
